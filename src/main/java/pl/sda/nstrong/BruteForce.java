package pl.sda.nstrong;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class BruteForce {

    private final int[][] routeMap;

    public BruteForce(int[][] routeMap) {
        this.routeMap = routeMap;
    }

    public int execute (){
//        int[] tableToPreparePermutate = {};
        ArrayList<Integer> temp = new ArrayList<>();
        for (int i = 0; i < routeMap.length; i++) {
            temp.add(i);
        }

        ArrayList<ArrayList<Integer>> permutations = PermutateFinder.permute(temp.stream().mapToInt(i -> i).toArray());

        int min = countRouteLength(permutations.get(0));
        List<Integer> bestSolution = permutations.get(0);
        for (List<Integer> solution : permutations){
            int sum = countRouteLength(solution);
            if (sum < min){
                min = sum;
                bestSolution = solution;
//                System.out.println(min);
//                System.out.println(Arrays.toString(bestSolution.toArray()));
//                System.out.println();
            }
        }

        return min;
    }

    private int countRouteLength(List<Integer> aSolution){
        int bufor = 0;
        for (int i = 0; i <aSolution.size()-1 ; i++) {
            int edge1 = aSolution.get(i);
            int edge2 = aSolution.get(i+1);

            bufor = bufor+ routeMap[edge1][edge2];
        }
        return bufor;
    }
}


