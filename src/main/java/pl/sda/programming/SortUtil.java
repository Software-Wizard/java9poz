package pl.sda.programming;

public class SortUtil {
    public static int[] insercionSort(int[] initial) {
        int[] ret = initial.clone();

        for (int i = 0; i < ret.length; i++) {
            Pair min = ArrayUtil.findMinimumAndIndex(i, ret);

            int temp = ret[i];
            ret[i] = min.getValue();
            ret[min.getIndex()] = temp;
        }
        return ret;
    }

    public static int[] qucikSort(int[] aArrayToSort) {
        int[] ret = aArrayToSort.clone();
        recursiveQucikSort(ret, 0, ret.length-1);
        return ret;
    }

    private static void recursiveQucikSort(int[] aArrayToSort, int aLeftIndex, int aRightIndex) {
        if (aLeftIndex >= aRightIndex) {
            // TODO dlaczego tutaj nie moze byc == -> kiedy przekazujemy pojedynczy element do tablicy lewy i prawy jest równy x. Do lewego podproblemu podajemy(x+1, x)
            return;
        }
        int pivot = aArrayToSort[aRightIndex];
        int pivotIndex = aRightIndex;
        int border = aLeftIndex;
        for (int i = border; i < pivotIndex; i++) {
            if (aArrayToSort[i] < pivot) {
                border++;
                if (i >= border) {
                    swap(aArrayToSort, i, border - 1);
                }
            }
        }
        swap(aArrayToSort, pivotIndex, border);
        recursiveQucikSort(aArrayToSort, aLeftIndex, border - 1);
        recursiveQucikSort(aArrayToSort, border + 1, aRightIndex);
    }


    public static int[] bubbleSort(int[] initial) {
        int[] ret = initial.clone();

        for (int i = 0; i < ret.length; i++) {
            for (int j = 0; j < ret.length -1; j++) {
                if(ret[j]>ret[j+1]){
                    swap(ret, j, j+1);
                }
            }
        }
        return ret;
    }

    private static void swap(int[] aArray, int a, int b) {
        int temp = aArray[a];
        aArray[a] = aArray[b];
        aArray[b] = temp;
    }
}
