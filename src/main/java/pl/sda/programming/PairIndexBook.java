package pl.sda.programming;

public class PairIndexBook {

    private final int index;
    private final Book book;

    public PairIndexBook(int index, Book book) {
        this.index = index;
        this.book = book;
    }

    public int getIndex() {
        return index;
    }

    public Book getBook() {
        return book;
    }
}
