package pl.sda.programming;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BookSortUtil {
    public static List<Book> insercionSort(List<Book> aBooksToSort) {
        List<Book> ret = new ArrayList<>();
        ret.addAll(aBooksToSort);

        for (int i = 0; i < ret.size(); i++) {
            PairIndexBook min = findMinimumAndIndex(i, ret);
            Collections.swap(ret, i, min.getIndex());
        }
        return ret;
    }

    public static PairIndexBook findMinimumAndIndex(int aStartIndex, List<Book> aBooks) {
        double min = aBooks.get(aStartIndex).getRating();
        int index = aStartIndex;
        for (int i = aStartIndex + 1; i < aBooks.size(); i++) {
            if (aBooks.get(i).getRating() < min) {
                min = aBooks.get(i).getRating();
                index = i;
            }
        }
        return new PairIndexBook(index, aBooks.get(index));
    }

    public static PairIndexBook findMinimumAndIndex(List<Book> aBooks) {
        double min = aBooks.get(0).getRating();
        int index = 0;
        for (int i = 1; i < aBooks.size(); i++) {
            if (aBooks.get(i).getRating() < min) {
                index = i;
                min = aBooks.get(i).getRating();
            }
        }
        return new PairIndexBook(index, aBooks.get(index));
    }

    private static void swap(int[] aArray, int a, int b) {
        int temp = aArray[a];
        aArray[a] = aArray[b];
        aArray[b] = temp;
    }


    public static List<Book> insercionSortByTitle(List<Book> aBooksToSort) {
        List<Book> ret = new ArrayList<>();
        ret.addAll(aBooksToSort);

        for (int i = 0; i < ret.size(); i++) {
            PairIndexBook min = findAlphabeticalFirstTitle(i, ret);
            Collections.swap(ret, i, min.getIndex());
        }
        return ret;
    }

    private static PairIndexBook findAlphabeticalFirstTitle(int aStartIndex, List<Book> aBooks) {
        String alphabeticalFirst = aBooks.get(aStartIndex).getTitle();
        int index = aStartIndex;
        for (int i = aStartIndex + 1; i < aBooks.size(); i++) {
            if (fisrtStringIsLower(aBooks.get(i).getTitle(), alphabeticalFirst)) {
                alphabeticalFirst = aBooks.get(i).getTitle();
                index = i;
            }
        }
        return new PairIndexBook(index, aBooks.get(index));
    }

    static boolean fisrtStringIsLower(String s1, String s2) {
        char[] c1 = s1.toCharArray();
        char[] c2 = s2.toCharArray();

        int length = Math.min(s1.length(), s2.length());

        for (int i = 0; i < length; i++) {
            int c1ByInt = Character.getNumericValue(c1[i]);
            int c2ByInt = Character.getNumericValue(c2[i]);

            if (c1ByInt < c2ByInt) {
                return true;
            } else if (c1ByInt == c2ByInt) {
                continue;
            } else {
                return false;
            }
        }
        return c1.length < c2.length ? true : false;
    }
}
