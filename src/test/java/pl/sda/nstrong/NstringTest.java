package pl.sda.nstrong;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class NstringTest {

    @Test
    void permutateFinderTest(){
        int[] table = {1,2,3,4,5,6,7,8,9,10};

//        System.out.println(Arrays.toString(PermutateFinder.permute(table).toArray()));
        System.out.println(PermutateFinder.permute(table).size());
    }

    @Test
    void nString(){
        int[][] routeMap = {
                {0,13,22,7,81,32,21,33,34,35},
                {13,0,56,66,77,88,22,99,11,14},
                {22,56,0,21,31,41,51,61,71,81},
                {7,66,21,0,43,34,75,65,97,74},
                {81,77,31,43,0,67,68,69,22,33},
                {32,88,41,34,67,0,76,37,48,19},
                {21,22,51,75,68,76,0,43,44,45},
                {33,99,61,65,69,37,43,0,64,73},
                {34,11,71,97,22,48,44,64,0,39},
                {35,14,81,74,33,19,45,73,39,0},
        };

        long start = System.currentTimeMillis();
        BruteForce bruteForce = new BruteForce(routeMap);
        System.out.println(bruteForce.execute());
        System.out.println(System.currentTimeMillis()-start);
    }


}