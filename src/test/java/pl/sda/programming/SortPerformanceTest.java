package pl.sda.programming;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class SortPerformanceTest {



    @Test
    void performanceTest(){
        int[] toSort= new int[10000];
        Random rand = new Random();
        for (int i = 0; i <toSort.length ; i++)
        {
            toSort[i] = rand.nextInt();
        }

        long startTime = System.currentTimeMillis();
        SortUtil.bubbleSort(toSort);
        long endTime = System.currentTimeMillis();
        System.out.println("Bubble sort: " + (endTime - startTime));

        startTime = System.currentTimeMillis();
        SortUtil.qucikSort(toSort);
        endTime = System.currentTimeMillis();
        System.out.println("quick sort: " + (endTime - startTime));

        startTime = System.currentTimeMillis();
        SortUtil.insercionSort(toSort);
        endTime = System.currentTimeMillis();
        System.out.println("insercion sort: " + (endTime - startTime));


        startTime = System.currentTimeMillis();
        Arrays.sort(toSort);
        endTime = System.currentTimeMillis();
        System.out.println("Java sort: " + (endTime - startTime));
    }

}