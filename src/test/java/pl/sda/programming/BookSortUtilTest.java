package pl.sda.programming;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookSortUtilTest {

    @Test
    void shoutReturnWorstBook(){
    //given
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Brent Weeks", "Czarny Pryzmat", 9.9));
        bookList.add(new Book("Jakub Ćwiek", "Kłamca", 7.5));
        bookList.add(new Book("Jarosław Grzędowicz", "Pan Lodowego Ogrodu TOM 1", 8.3));
        bookList.add(new Book("Andrzej Sapkowski", "Pani Jeziorna", 1.0));
        bookList.add(new Book("Marcin Przybyłek", "Gamedec Zabaweczki", 7.1));

        //when
        PairIndexBook ret = BookSortUtil.findMinimumAndIndex(bookList);

    //then
        assertEquals(new Book("Andrzej Sapkowski", "Pani Jeziorna", 1.0) ,ret.getBook());
        assertEquals(3, ret.getIndex());
    }

    @Test
    void shoutReturnWorstBookAndIndex(){
    //given
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Andrzej Sapkowski", "Pani Jeziorna", 1.0));
        bookList.add(new Book("Brent Weeks", "Czarny Pryzmat", 9.9));
        bookList.add(new Book("Jakub Ćwiek", "Kłamca", 7.5));
        bookList.add(new Book("Jarosław Grzędowicz", "Pan Lodowego Ogrodu TOM 1", 8.3));
        bookList.add(new Book("Marcin Przybyłek", "Gamedec Zabaweczki", 7.1));

        //when
        PairIndexBook ret = BookSortUtil.findMinimumAndIndex(2, bookList);

    //then
        assertEquals(new Book("Marcin Przybyłek", "Gamedec Zabaweczki", 7.1) ,ret.getBook());
        assertEquals(4, ret.getIndex());
    }

    @Test
    void shouldReturnSortedList(){
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Andrzej Sapkowski", "Pani Jeziorna", 1.0));
        bookList.add(new Book("Brent Weeks", "Czarny Pryzmat", 9.9));
        bookList.add(new Book("Jakub Ćwiek", "Kłamca", 7.5));
        bookList.add(new Book("Jarosław Grzędowicz", "Pan Lodowego Ogrodu TOM 1", 8.3));
        bookList.add(new Book("Marcin Przybyłek", "Gamedec Zabaweczki", 7.1));

        List<Book> expected = new ArrayList<>();
        expected.add(new Book("Andrzej Sapkowski", "Pani Jeziorna", 1.0));
        expected.add(new Book("Marcin Przybyłek", "Gamedec Zabaweczki", 7.1));
        expected.add(new Book("Jakub Ćwiek", "Kłamca", 7.5));
        expected.add(new Book("Jarosław Grzędowicz", "Pan Lodowego Ogrodu TOM 1", 8.3));
        expected.add(new Book("Brent Weeks", "Czarny Pryzmat", 9.9));

        List<Book> ret = BookSortUtil.insercionSort(bookList);

        assertEquals(expected, ret);
    }
    @Test
    void shouldReturnSortedByTitleList(){
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Andrzej Sapkowski", "Pani Jeziorna", 1.0));
        bookList.add(new Book("Brent Weeks", "Czarny Pryzmat", 9.9));
        bookList.add(new Book("Jakub Ćwiek", "Kłamca", 7.5));
        bookList.add(new Book("Jarosław Grzędowicz", "Pan Lodowego Ogrodu TOM 1", 8.3));
        bookList.add(new Book("Marcin Przybyłek", "Gamedec Zabaweczki", 7.1));

        List<Book> expected = new ArrayList<>();
        expected.add(new Book("Brent Weeks", "Czarny Pryzmat", 9.9));
        expected.add(new Book("Marcin Przybyłek", "Gamedec Zabaweczki", 7.1));
        expected.add(new Book("Jakub Ćwiek", "Kłamca", 7.5));
        expected.add(new Book("Jarosław Grzędowicz", "Pan Lodowego Ogrodu TOM 1", 8.3));
        expected.add(new Book("Andrzej Sapkowski", "Pani Jeziorna", 1.0));

        List<Book> ret = BookSortUtil.insercionSortByTitle(bookList);

        assertEquals(expected, ret);
    }

    @Test
    void shouldRecognizeAlphabeticalOrder(){
        assertTrue(BookSortUtil.fisrtStringIsLower("a","b"));
        assertFalse(BookSortUtil.fisrtStringIsLower("b","a"));
        assertTrue(BookSortUtil.fisrtStringIsLower("Pan","Pani D"));
        assertFalse(BookSortUtil.fisrtStringIsLower("abc","abc"));
    }
}