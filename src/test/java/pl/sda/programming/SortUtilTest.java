package pl.sda.programming;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SortUtilTest {

    @Test
    void shouldReturnSortedAscArray() {
        //given
        int[] initial = {9, 1, 3, 7, 5, 2, 6, 8, 4};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        //when
        int[] sorted = SortUtil.insercionSort(initial);

        //then
        assertArrayEquals(expected, sorted);
        int[] notSorted = {9, 1, 3, 7, 5, 2, 6, 8, 4};
        assertArrayEquals(notSorted, initial);
    }

    @Test
    void shouldReturnSortedByQuickSort() {
        //given
        int[] initial = {9, 1, 3, 7, 5, 2, 6, 8, 4};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        //when
        int[] sorted = SortUtil.qucikSort(initial);
        //then
        assertArrayEquals(expected, sorted);
        int[] notSorted = {9, 1, 3, 7, 5, 2, 6, 8, 4};
        assertArrayEquals(notSorted, initial);
    }

    @Test
    void shouldReturnSortedByBubbleSort() {
        //given
        int[] initial = {9, 1, 3, 7, 5, 2, 6, 8, 4};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        //when
        int[] sorted = SortUtil.bubbleSort(initial);
        //then
        assertArrayEquals(expected, sorted);
        int[] notSorted = {9, 1, 3, 7, 5, 2, 6, 8, 4};
        assertArrayEquals(notSorted, initial);
    }
}
